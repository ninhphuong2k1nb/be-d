<?php 

require_once('app/Controllers/Admin/BackendController.php');
require_once('app/Models/Product.php');
require_once('core/Flash.php');
require_once('app/Requests/ProductsRequest.php');
require_once('core/Storage.php');
class ProductsController extends BackendController{
    private $products;
    private $key ='products';
    public function __construct(){
        $this->products= new Product();
    }
    public function index()
    {
        return $this->view('product/index.php');
    }
    

}
?>