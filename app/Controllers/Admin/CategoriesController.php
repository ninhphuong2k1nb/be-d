<?php 

require_once('app/Controllers/Admin/BackendController.php');
require_once('app/Models/Categories.php');
require_once('core/Flash.php');
require_once('app/Requests/CategoriesRequest.php');

class CategoriesController extends BackendController
{
    public function index()
    {
        return $this->view('categories/index.php');
    }
}
?>